<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sactpro | Register</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?=base_url()?>public/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=base_url()?>public/assets/font-awesome/css/font-awesome.min.css">
		      <link rel="stylesheet" href="<?=base_url()?>public/assets/css/form-elements.css">
        <link rel="stylesheet" href="<?=base_url()?>/public/assets/css/style.css">
    </head>

    <body>


        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">
                <div class="container">

                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 form-box">

                        	<!-- <form role="form" action="Settings/do_multi_upload" method="post" class="registration-form"> -->
                          <?php echo form_open_multipart('Settings/do_multi_upload', 'class="registration-form"'); ?>


                        		<fieldset>
		                        	<div class="form-top">
		                        		<div class="form-top-left">
		                        			<h3>Step 1 / 3</h3>
		                            		<p>Teaching Preferences</p>
		                        		</div>
		                        		<div class="form-top-right">
                                  <i class="fa fa-key"></i>
		                        		</div>
		                            </div>
		                            <div class="form-bottom">
                                  <div class="form-row">

                                  <div class="form-group col-md-12" style="padding-left: 0">
                                    <p class="pop_label" for="form-first-name">Select Tutoring area</p>
                                      <select class="form-control" id="t_state" name="t_state" placeholder="Enter your current city" required autofocus>
                                          <option id="t_state" name="t_state" selected="true" disabled="disabled">Select State</option>
                                          <option id="t_state" name="t_state">Andhra Pradesh</option>
                                          <option id="t_state" name="t_state">Chennai</option>
                                          <option id="t_state" name="t_state">Delhi</option>
                                          <option id="t_state" name="t_state">Haryana</option>
                                          <option id="t_state" name="t_state">Karnataka</option>
                                          <option id="t_state" name="t_state">Kerala</option>
                                          <option id="t_state" name="t_state">Madhya Pradesh</option>
                                          <option id="t_state" name="t_state">Maharastra</option>
                                          <option id="t_state" name="t_state">Orrisa</option>
                                          <option id="t_state" name="t_state">Punjab</option>
                                          <option id="t_state" name="t_state">Rajasthan</option>
                                          <option id="t_state" name="t_state">Telangana</option>
                                          <option id="t_state" name="t_state">TamilNadu</option>
                                          <option id="t_state" name="t_state">West Bengal</option>
                                      </select>
                                    </div>
                                    <div class="form-group col-md-6" style="padding-left: 0">
                                      <p class="pop_label" for="form-last-name">Board</p>
                                      <label class="checkbox-inline"><input id="board" name="board[]" type="checkbox" value="SSC">SSC</label>
                                      <label class="checkbox-inline"><input id="board" name="board[]" type="checkbox" value="CBSE">CBSE</label>
                                      <label class="checkbox-inline"><input id="board" name="board[]" type="checkbox" value="ICSE">ICSE</label>
                                      <label class="checkbox-inline"><input id="board" name="board[]" type="checkbox" value="IGCSE">IGCSE</label>
                                    </div>
                                    <div class="form-group col-md-6" style="padding-left: 0">
                                      <p class="pop_label" for="form-last-name">Mode of Tutoring</p>
                                      <label class="checkbox-inline"><input id="mode" name="mode[]" type="checkbox" value="Online">Online</label>
                                      <label class="checkbox-inline"><input id="mode" name="mode[]" type="checkbox" value="Offline">Offline</label>
                                    </div>
                                    <div class="form-group col-md-6" style="padding-left: 0">
                                      <p class="pop_label" for="form-about-yourself">Preferred Place</p>
                                      <label class="checkbox-inline"><input id="teaching-place" name="teach-plc[]" type="checkbox" value="Tutor Home">Your Home</label>
                                      <label class="checkbox-inline"><input id="teaching-place" name="teach-plc[]" type="checkbox" value="Student Home">Student Home</label>
                                    </div>
                                    <div class="form-group col-md-6" style="padding-left: 0">
                                      <p class="pop_label" for="form-about-yourself">Interested in teaching at schools?</p>
                                      <label class="radio-inline"><input type="radio" name="teach_inst" value="YES">Yes</label>
                                      <label class="radio-inline"><input type="radio" name="teach_inst" value="NO">No</label>
                                    </div>
				                        <button type="button" class="btn btn-next">Next</button>
                              </div>
				                    </div>
			                    </fieldset>

			                    <fieldset>
		                        	<div class="form-top">
		                        		<div class="form-top-left">
		                        			<h3>Step 2 / 3</h3>
		                            		<p>Select Subject and Class</p>
		                        		</div>
		                        		<div class="form-top-right">
		                        		</div>
		                            </div>
		                            <div class="form-bottom">
                                  <p class="pop_label" for="form-about-yourself">Class</p>
                                  <div class="form-group">
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="PrePri">Pre-Primary</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="1st">1st</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="2nd">2nd</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="3rd">3rd</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="4th">4th</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="5th">5th</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="6th">6th</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="7th">7th</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="8th">8th</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="9th">9th</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="10th">10th</label>
                                  </div>
                                  <div class="form-group">
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="Inter1Y">Inter 1st Year</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="Inter2Y">Inter 2nd Year</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="IIT-JEE">IIT-JEE</label>
                                    <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="IIT-J2E">IIT-J2E</label>
                                  </div>
                                <div class="form-group">
                                  <label class="checkbox-inline"><input type="checkbox" value="">B.Tech</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">BBA</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">BSC</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">BCA</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">MBA</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">MCA</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">M.Tech</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">LLB</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">NEET</label>
                                  <label class="checkbox-inline"><input type="checkbox" value="">MBBS</label>
				                        </div>
                                <div class="form-group">
                                  <p class="pop_label" for="form-about-yourself">Subject</p>
                                  <input type="text" name="subject" placeholder="Maths, Physics.." class="form-google-plus form-control" id="subject">
                                </div>
				                        <button type="button" class="btn btn-previous">Previous</button>
				                        <button type="button" class="btn btn-next">Next</button>
				                    </div>
			                    </fieldset>

                          <fieldset>
		                        	<div class="form-top">
		                        		<div class="form-top-left">
		                        			<h3>Step 3 / 3</h3>
		                            		<p>Profile Preferences</p>
		                        		</div>
		                        		<div class="form-top-right">
                                  <i class="fa fa-user"></i>
		                        		</div>
		                            </div>
		                            <div class="form-bottom">
                                  <div class="form-row">
                                    <div class="form-group col-md-12" style="padding-left: 0 !important;">
                                      <label for="date">Upload your Profile Photo</label>
                                      <input type="file" name="profile_pic[]" size="200000"required autofocus />
                                    </div>
                                    <div class="form-group col-md-6" style="padding-left: 0 !important;">
                                      <p class="pop_label" for="form-first-name">Gender</p>
                                        <select class="form-control" id="gender" name="gender" placeholder="Enter your current city" required autofocus>
                                            <option id="gender" name="gender" selected="true" disabled="disabled">Select Gender</option>
                                            <option id="gender" name="gender">Male</option>
                                            <option id="gender" name="gender">Female</option>
                                        </select>
                                        <p class="pop_label" for="teach-exp">Teaching Experience</p>
                                        <input type="text" name="teach-exp" placeholder="Ex: 1 Year" class="form-google-plus form-control" id="teach-exp" required autofocus>
                                        <p class="pop_label" for="form-about-yourself">Home Address</p>
                                        <input type="text" name="address" placeholder="Hno: 10-1, Rd: 7, Pt: 10 ex colony" class="form-google-plus form-control" id="address" required autofocus>
                                    </div>
                                    <div class="form-group col-md-6" style="padding: 0 !important;">
                                      <p class="pop_label" for="form-google-plus">City</p>
                                      <input type="text" name="city" placeholder="City" class="form-google-plus form-control" id="city" required autofocus>
                                      <p class="pop_label" for="form-first-name">State</p>
                                        <select class="form-control" id="state" name="state" placeholder="Enter your current city" required autofocus>
                                            <option id="state" name="state" selected="true" disabled="disabled">Select State</option>
                                            <option id="state" name="state">Andhra Pradesh</option>
                                            <option id="state" name="state">Chennai</option>
                                            <option id="state" name="state">Delhi</option>
                                            <option id="state" name="state">Haryana</option>
                                            <option id="state" name="state">Karnataka</option>
                                            <option id="state" name="state">Kerala</option>
                                            <option id="state" name="state">Madhya Pradesh</option>
                                            <option id="state" name="state">Maharastra</option>
                                            <option id="state" name="state">Orrisa</option>
                                            <option id="state" name="state">Punjab</option>
                                            <option id="state" name="state">Rajasthan</option>
                                            <option id="state" name="state">Telangana</option>
                                            <option id="state" name="state">TamilNadu</option>
                                            <option id="state" name="state">West Bengal</option>
                                        </select>
                                        <p class="pop_label" for="form-google-plus">Pincode</p>
                                        <input type="text" name="pincode" placeholder="Pincode" class="form-google-plus form-control" id="pincode" required autofocus>
                                    </div>
                                  </div>
				                        <button type="button" class="btn btn-previous">Previous</button>
				                        <!-- <button type="submit" class="btn">Submit</button> -->
                                <input id="prof_update_btn" type="submit" class="btn btn-primary" value="Submit" name="submit_form"/>
				                    </div>
			                    </fieldset>

		                    </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>


        <!-- Javascript -->
        <script src="<?=base_url()?>public/assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?=base_url()?>public/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>public/assets/js/jquery.backstretch.min.js"></script>
        <script src="<?=base_url()?>public/assets/js/retina-1.1.0.min.js"></script>
        <script src="<?=base_url()?>public/assets/js/scripts.js"></script>

        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
