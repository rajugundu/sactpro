<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index() {

        if (isset($_POST['login'])) {
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == TRUE) {

                $username = $_POST['username'];
                $password = $_POST['password'];

                $this->db->select('*');
                $this->db->from('users');
                $this->db->where(array('username' => $username, 'password' => $password));

                $query = $this->db->get();

                $user = $query->row();

                if ($user->email) {
                    $this->session->set_flashdata('success', 'SuccessFully logged in');
                    $_SESSION['user_logged'] = TRUE;
                    $_SESSION['username'] = $user->username;
										$_SESSION['email'] = $user->email;
                    redirect("/user/tutor_profile", 'refresh');

                } else {
                    $_SESSION['user_logged'] = FALSE;
                    $this->session->set_flashdata('error', 'Please register and try login again');
                    redirect("", 'refresh');
                    // echo 'error';
                }
            }
        }

        if (isset($_POST['login_student'])) {
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == TRUE) {

                $username = $_POST['username'];
                $password = $_POST['password'];

                $this->db->select('*');
                $this->db->from('student_registration');
                $this->db->where(array('username' => $username, 'password' => $password));

                $query = $this->db->get();

                $user = $query->row();

                if ($user->email) {
                    $this->session->set_flashdata('success', 'SuccessFully logged in');
                    $this->session->userdata();
                    $_SESSION['user_logged'] = TRUE;
                    $_SESSION['username'] = $user->username;
                    $_SESSION['password'] = $user->password;
                    redirect("/user/student_profile/", 'refresh');

                } else {
                    $this->form_validation->set_message('rule', 'Error Message');
                    $_SESSION['user_logged'] = FALSE;
                    $this->session->set_flashdata('error', 'No Such account with the logins. Please register and login again.');
                    redirect("", 'refresh');
                }
            }
        }

        $this->load->view('Login');
    }


    public function logout() {
        unset($_SESSION);
        session_destroy();
        redirect('', 'refresh');
    }
}
