<?php
// Define variables for SEO
$pageTitle = 'Register | SactPro';
$pageDescription = 'Register with Sactpro and find relevant requirements for your skills.';
$pageCanonical = 'http://sactpro.com/register';

 ?>
<?php include_once("Header.php"); ?>

<div class="form-body">
    <div class="container">
        <div class="row">
            <?php if(isset($_SESSION['success'])) { ?>
                <div class="alert alert-success"><?php echo $_SESSION['success']; ?> </div>
            <?php
            }
            ?>
            <div id="tutor-border" class="col-lg-6 col-md-12">
                <h4>REGISTER AS TUTOR</h4>
                <form action="" method="POST">
                    <div class="form-group">
                        <!-- <label for="username">Full Name</label> -->
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter Full name" required autofocus>
                    </div>
                    <div class="form-group">
                        <!-- <label for="number">Phone Number</label> -->
                        <input type="text" class="form-control" id="number" name="number" placeholder="Enter Phone number" required autofocus>
                    </div>
                    <div class="form-group">
                        <!-- <label for="email">Email address</label> -->
                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" required autofocus>
                        <!-- <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                    </div>
                    <div class="form-group">
                        <!-- <label for="password">Password</label> -->
                        <input type="password" class="form-control" id="password" name="password" placeholder="Create a password" required autofocus>
                    </div>
                    <button type="submit" class="btn btn-primary" name="register">Register Now</button>
                </form>
            </div>
            <div class="col-lg-6 col-md-12">
            <h4>REGISTER AS STUDENT</h4>
                <form action="" method="POST">
                    <div class="form-group">
                        <!-- <label for="username">Full Name</label> -->
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter Full name (with no spaces)" required autofocus>
                    </div>
                    <div class="form-group">
                        <!-- <label for="number">Phone Number</label> -->
                        <input type="text" class="form-control" id="number" name="number" placeholder="Enter Phone number" required autofocus>
                    </div>
                    <div class="form-group">
                        <!-- <label for="email">Email address</label> -->
                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" required autofocus>
                        <!-- <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                    </div>
                    <div class="form-group">
                        <!-- <label for="password">Password</label> -->
                        <input type="password" class="form-control" id="password" name="password" placeholder="Create a password" required autofocus>
                    </div>
                    <div class="form-group">
                        <!-- <label for="subject">Subject</label> -->
                        <!-- <input type="select" class="form-control" id="subject" name="subject" placeholder="Select subject"> -->
                        <select class="form-control" id="class" name="class" placeholder="Select Class">
                            <option id="class" name="class" selected="true" disabled="disabled">Select Class</option>
                            <option id="class" name="class">All boards(KG Vl to X)</option>
                            <option id="class" name="class">Intermediate (Xl,Xll)</option>
                            <option id="class" name="class">AIEEE/IIT-JEE</option>
                            <option id="class" name="class">Engineering subjects</option>
                            <option id="class" name="class">NEET/AIIMS</option>
                            <option id="class" name="class">BSC/BCA</option>
                            <option id="class" name="class">All Spoken language</option>
                            <option id="class" name="class">BCOM-BAF</option>
                            <option id="class" name="class">CPT-IPCC/ATC</option>
                            <option id="class" name="class">MBA/BBA/BBM</option>
                            <option id="class" name="class">BBM-BMS</option>
                            <option id="class" name="class">AIEEE/IIT-JEE</option>
                            <option id="class" name="class">TOEFL/IELTS</option>
                            <option id="class" name="class">GRE/GMAT</option>
                            <option id="class" name="class">MBBS/BDS</option>
                            <option id="class" name="class">DIPLOMA subjects</option>
                            <option id="class" name="class">Handwriting classes</option>
                            <option id="class" name="class">Competitive Exams</option>
                            <option id="class" name="class">Computer courses</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <!-- <label for="subject">Subject</label> -->
                        <!-- <input type="select" class="form-control" id="subject" name="subject" placeholder="Select subject"> -->
                        <select class="form-control" id="subject" name="subject" placeholder="Start Date">
                            <option id="subject" name="subject" selected="true" disabled="disabled">Select Subject</option>
                            <option id="subject" name="subject">Telugu</option>
                            <option id="subject" name="subject">Hindi</option>
                            <option id="subject" name="subject">English</option>
                            <option id="subject" name="subject">Karate</option>
                            <option id="subject" name="subject">Maths</option>
                            <option id="subject" name="subject">Chemistry</option>
                            <option id="subject" name="subject">(1st - 12th)Physics</option>
                            <option id="subject" name="subject">PolyCet</option>
                            <option id="subject" name="subject">Intermeditate 1st year</option>
                            <option id="subject" name="subject">Intermeditate 2nd year</option>
                            <option id="subject" name="subject">M1</option>
                            <option id="subject" name="subject">ICet</option>
                            <option id="subject" name="subject">Ielts</option>
                            <option id="subject" name="subject">TOFEL</option>
                            <option id="subject" name="subject">Gre</option>
                            <option id="subject" name="subject">CAT</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <!-- <label for="subject">Subject</label> -->
                        <!-- <input type="select" class="form-control" id="subject" name="subject" placeholder="Select subject"> -->
                        <select class="form-control" id="timing" name="timing" placeholder="Prefered Timing">
                            <option id="timing" name="timing" selected="true" disabled="disabled">Prefered Timing</option>
                            <option id="timing" name="timing">Morning (8:am-12pm)</option>
                            <option id="timing" name="timing">Afternoon (1:pm-4pm)</option>
                            <option id="timing" name="timing">Evening (5:pm-7pm)</option>
                            <option id="timing" name="timing">Night (8:pm-10pm)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <!-- <label for="username">Full Name</label> -->
                        <!-- <input type="text" class="form-control" id="city" name="city" placeholder="Enter your current city" required autofocus> -->
                        <select class="form-control" id="city" name="city" placeholder="Enter your current city" required autofocus>
                            <option id="city" name="city" selected="true" disabled="disabled">Select State</option>
                            <option id="city" name="city">Andhra Pradesh</option>
                            <option id="city" name="city">Chennai</option>
                            <option id="city" name="city">Delhi</option>
                            <option id="city" name="city">Haryana</option>
                            <option id="city" name="city">Karnataka</option>
                            <option id="city" name="city">Kerala</option>
                            <option id="city" name="city">Madhya Pradesh</option>
                            <option id="city" name="city">Maharastra</option>
                            <option id="city" name="city">Orrisa</option>
                            <option id="city" name="city">Punjab</option>
                            <option id="city" name="city">Rajasthan</option>
                            <option id="city" name="city">Telangana</option>
                            <option id="city" name="city">TamilNadu</option>
                            <option id="city" name="city">West Bengal</option>
                        </select>
                    </div>

                     <div class="form-group">
                        <textarea class="form-control" id="address" name="address" rows="3" placeholder="Enter full address" required autofocus></textarea>
                    </div>

                    <div class="checkbox">
                        <label><input type="checkbox" value=""> I have read and accept <a class="termslink" href="<?=base_url()?>terms">Terms & Conditions</a></label>
                    </div>
                    <button type="submit" class="btn btn-primary" name="student_register">Register Now</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include_once("Footer.php"); ?>
