

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer-logo">
                    <a href="<?=base_url()?>"><img src="<?=base_url()?>public/img/main_logo.png" /></a>
                    <form action="" method="POST">
                        <div class="form-group">
                            <input type="text" class="form-control" id="username" name="username" placeholder="Join us for our newsletters">
                            <button type="submit" class="btn btn-primary" name="news-letter">Join</button>
                        </div>
                    </form>
                    <div class="social-connect">
                        <h5>FOLLOW US ON</h5>
                        <a href="https://www.facebook.com/profile.php?id=100021622300716"><img src="<?=base_url()?>public/social/facebook.svg" /></a>
                        <a href="https://www.instagram.com/sactutorials/"><img src="<?=base_url()?>public/social/instagram.svg" /></a>
                        <!--  <a href="linkedin.com"><img src="<?=base_url()?>public/social/linkedin.svg" /></a> -->
                        <a href="https://plus.google.com/102383589183969063949"><img src="<?=base_url()?>public/social/googleplus.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
                <h5>QUICK LINKS</h5>
                <div class="footer-links">
                    <p><a href="<?=base_url()?>about" title="About Us">About Us</a></p>
                    <p><a href="<?=base_url()?>contact" title="Contact Us">Contact Us</a></p>
                    <p><a href="<?=base_url()?>Whysactpro" title="Why Sactpro">Why sactpro</a></p>
                    <p><a href="<?=base_url()?>Donate" title="Donate">Donate</a></p>
                </div>
                <h5>OFFICE TIMINGS</h5>
                <div class="timings">
                    <p>Mon - Sat: 10am to 9pm</p>
                    <p>Sun: 10am to 2pm</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="copyright">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="copyright-text">
                    <p>Copyright 2018 | SactPro Online Services Private limited. All right reserved.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="copyright-links">
                    <a href="<?=base_url()?>privacy" title="Privacy Policy">Privacy Policy</a>
                    <a href="<?=base_url()?>terms" title="Terms & Conditions">Terms & Conditions</a>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/js/scripts.js"></script>

   <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
