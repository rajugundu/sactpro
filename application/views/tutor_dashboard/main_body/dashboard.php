<?php if(isset($_SESSION['Purchased'])) { ?>
  <div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <?php echo $_SESSION['Purchased']; ?>
  </div>
<?php }?>

<div data-spy="scroll" id="scroll-modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="top-content">
        <div class="inner-bg">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <?php echo form_open_multipart('Settings/do_multi_upload');?>

                <fieldset>
                  <div class="form-top">
                    <div class="form-top-left">
                        <p>Teaching Preferences</p>
                    </div>
                  </div>
                  <div class="form-bottom">
                    <div class="form-group">
                      <p class="pop_label" for="form-first-name">Select Tutoring area</p>
                        <select class="form-control" id="t_state" name="t_state" placeholder="Enter your current city" required autofocus>
                            <option id="t_state" name="t_state" selected="true" disabled="disabled">Select State</option>
                            <option id="t_state" name="t_state">Andhra Pradesh</option>
                            <option id="t_state" name="t_state">Chennai</option>
                            <option id="t_state" name="t_state">Delhi</option>
                            <option id="t_state" name="t_state">Haryana</option>
                            <option id="t_state" name="t_state">Karnataka</option>
                            <option id="t_state" name="t_state">Kerala</option>
                            <option id="t_state" name="t_state">Madhya Pradesh</option>
                            <option id="t_state" name="t_state">Maharastra</option>
                            <option id="t_state" name="t_state">Orrisa</option>
                            <option id="t_state" name="t_state">Punjab</option>
                            <option id="t_state" name="t_state">Rajasthan</option>
                            <option id="t_state" name="t_state">Telangana</option>
                            <option id="t_state" name="t_state">TamilNadu</option>
                            <option id="t_state" name="t_state">West Bengal</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <p class="pop_label" for="form-last-name">Board</p>
                        <label class="checkbox-inline"><input id="board" name="board[]" type="checkbox" value="SSC">SSC</label>
                        <label class="checkbox-inline"><input id="board" name="board[]" type="checkbox" value="CBSE">CBSE</label>
                        <label class="checkbox-inline"><input id="board" name="board[]" type="checkbox" value="ICSE">ICSE</label>
                        <label class="checkbox-inline"><input id="board" name="board[]" type="checkbox" value="IGCSE">IGCSE</label>
                      </div>
                      <div class="form-group">
                        <p class="pop_label" for="form-last-name">Mode of Tutoring</p>
                        <label class="checkbox-inline"><input id="mode" name="mode[]" type="checkbox" value="Online">Online</label>
                        <label class="checkbox-inline"><input id="mode" name="mode[]" type="checkbox" value="Offline">Offline</label>
                      </div>
                      <div class="form-group">
                        <p class="pop_label" for="form-about-yourself">Preferred Place</p>
                        <label class="checkbox-inline"><input id="teaching-place" name="teach-plc[]" type="checkbox" value="Tutor Home">Your Home</label>
                        <label class="checkbox-inline"><input id="teaching-place" name="teach-plc[]" type="checkbox" value="Student Home">Student Home</label>
                      </div>
                      <div class="form-group">
                        <p class="pop_label" for="form-about-yourself">Interested in teaching at schools?</p>
                        <label class="radio-inline"><input type="radio" name="teach_inst" value="YES">Yes</label>
                        <label class="radio-inline"><input type="radio" name="teach_inst" value="NO">No</label>
                      </div>
                  </div>
                </fieldset>
              </div>
              <div class="col-lg-6 col-md-6">
                <fieldset>
                  <div class="form-top">
                    <div class="form-top-left">
                      <p>Select Subject and Class</p>
                    </div>
                    </div>
                    <div class="form-bottom">
                      <p class="pop_label" for="form-about-yourself">Class</p>
                    <div class="form-group">
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="PrePri">Pre-Primary</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="1st">1st</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="2nd">2nd</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="3rd">3rd</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="4th">4th</label>
                    </div>
                    <div class="form-group">
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="5th">5th</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="6th">6th</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="7th">7th</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="8th">8th</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="9th">9th</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="10th">10th</label>
                    </div>
                    <div class="form-group">
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="Inter1Y">Inter 1st Year</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="Inter2Y">Inter 2nd Year</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="IIT-JEE">IIT-JEE</label>
                    </div>
                    <div class="form-group">
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="IIT-J2E">IIT-J2E</label>
                    </div>
                    <div class="form-group">
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="Diploma">Diploma</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="Polytechnic">Polytechnic</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="Languages">Languages</label>
                    </div>
                    <div class="form-group">
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="BTech">B.Tech</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="BBA">BBA</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="BSC">BSC</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="BCA">BCA</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="MBA">MBA</label>
                    </div>
                    <div class="form-group">
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="MCA">MCA</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="MTech">M.Tech</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="LLB">LLB</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="NEET">NEET</label>
                      <label class="checkbox-inline"><input id="class" name="class[]" type="checkbox" value="MBBS">MBBS</label>
                    </div>
                    <p class="pop_label" for="form-about-yourself">Subject</p>
                    <input type="text" name="subject" placeholder="Maths, Physics.." class="form-google-plus form-control" id="subject" required autofocus>
                  </div>
                </fieldset>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <fieldset>
                    <div class="form-top">
                      <div class="form-top-left">
                          <p>Profile Preferences</p>
                      </div>
                      </div>
                      <div class="form-bottom">
                        <div class="form-row">
                          <div class="form-group col-md-6" style="padding: 10px !important;">
                            <p class="pop_label" for="form-first-name">Gender</p>
                              <select class="form-control" id="gender" name="gender" placeholder="Enter your current city" required autofocus>
                                  <option id="gender" name="gender" selected="true" disabled="disabled">Select Gender</option>
                                  <option id="gender" name="gender">Male</option>
                                  <option id="gender" name="gender">Female</option>
                              </select>
                            <p class="pop_label" for="teach-exp">Teaching Experience</p>
                            <input type="text" name="teach-exp" placeholder="Ex: 1 Year" class="form-google-plus form-control" id="teach-exp" required autofocus>
                            <p class="pop_label" for="form-about-yourself">Home Address</p>
                            <input type="text" name="address" placeholder="Hno: 10-1, Rd: 7, Pt: 10 ex colony" class="form-google-plus form-control" id="address" required autofocus>
                          </div>
                          <div class="form-group col-md-6" style="padding: 10px !important;">
                            <p class="pop_label" for="form-first-name">State</p>
                              <select class="form-control" id="state" name="state" placeholder="Enter your current city" required autofocus>
                                  <option id="state" name="state" selected="true" disabled="disabled">Select State</option>
                                  <option id="state" name="state">Andhra Pradesh</option>
                                  <option id="state" name="state">Chennai</option>
                                  <option id="state" name="state">Delhi</option>
                                  <option id="state" name="state">Haryana</option>
                                  <option id="state" name="state">Karnataka</option>
                                  <option id="state" name="state">Kerala</option>
                                  <option id="state" name="state">Madhya Pradesh</option>
                                  <option id="state" name="state">Maharastra</option>
                                  <option id="state" name="state">Orrisa</option>
                                  <option id="state" name="state">Punjab</option>
                                  <option id="state" name="state">Rajasthan</option>
                                  <option id="state" name="state">Telangana</option>
                                  <option id="state" name="state">TamilNadu</option>
                                  <option id="state" name="state">West Bengal</option>
                              </select>
                            <p class="pop_label" for="form-google-plus">City</p>
                            <input type="text" name="city" placeholder="City" class="form-google-plus form-control" id="city" required autofocus>
                            <p class="pop_label" for="form-google-plus">Pincode</p>
                            <input type="text" name="pincode" placeholder="Pincode" class="form-google-plus form-control" id="pincode" required autofocus>
                          </div>
                          <div class="form-group col-md-12" style="padding-left: 0 !important;">
                            <p class="pop_label" for="date">Upload your Profile Photo</p>
                            <input style="border-bottom: 0 !important;" type="file" name="profile_pic[]" size="200000" required autofocus />
                          </div>
                          <div class="form-group col-md-12" style="padding-left: 0 !important;">
                            <p class="pop_label" for="date">For verification purposes, please upload any of document like aadhar, pancard or driving licence</p>
                            <input style="border-bottom: 0 !important;" type="file" name="profile_pic[]" size="200000" />
                          </div>
                        </div>
                  </div>
                </fieldset>
              </div>
            </div>
            <p style="color: red;">*Please fill all fields</p>
            <input id="prof_update_btn" type="submit" class="btn btn-primary" value="Update" name="submit_form"/>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




<div id="dashboard" class="tab-pane fade in active">
    <div class="leads-title">
        <h5>Current Requirments</h5>
    </div>
    <div class="leads-desc-title">
        <div class="row">
          <div class="col-lg-1">
              <div class="lead-text">
                  <p>Job ID</p>
              </div>
          </div>
            <div class="col-lg-2">
                <div class="lead-text">
                    <p>Name</p>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="lead-text">
                    <p>Location</p>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="lead-text">
                    <p>Class & Board</p>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="lead-text">
                    <p>Subject</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="lead-text">
                    <p>Coins</p>
                </div>
            </div>
        </div>
    </div>
    <div>
      <!-- <select id="test-dropdown" onchange="choice1(this)">
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
      </select> -->
      <?php $student_details = $this->db->get_where('student_registration')->result();
        foreach ($records->result() as $student_views ) {
           $job_id = $student_views->id;
          ?>
            <div class="leads">
              <div class="row" id="test" data-value="1">
                <div class="col-lg-1">
                    <div class="profile-img">
                        <p style="text-align: center;"><?php echo $student_views->id; ?></p>
                    </div>
                </div>
                  <div class="col-lg-2">
                      <div class="profile-img">
                          <p><?php echo $student_views->username; ?></p>
                      </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="phone">
                        <p><?php echo $student_views->city ?></p>
                      </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="subject">
                          <p><?php echo $student_views->class; ?></p>
                      </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="time">
                          <p><?php echo $student_views->subject; ?></p>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="lead-text">
                          <p>
                          <?php
                          if ($data['paid'] < 100) {
                            echo 'Buy credits';
                          } else {
                            ?>
                            <a href="<?php echo base_url("Settings/edit/" .$job_id) ?>">Get for 100 coins <i class="fas fa-arrow-alt-circle-right"></i></a>
                            <?php
                          }
                          ?>
                          </p>
                      </div>
                  </div>
              </div>
            </div>
        <?php } ?>
    </div>

    <ul class="pagination">
        <?php echo $this->pagination->create_links(); ?>
    </ul>




</div>


<script>
// function choice1(select) {
//   var test = document.getElementById("test-dropdown");
//   alert(select.options[select.selectedIndex].text);
// }
var id = document.getElementById("test-dropdown").innerHTML;
function test() {
  alert (this.id);
  // alert(document.getElementById("test").innerHTML);
}
</script>
