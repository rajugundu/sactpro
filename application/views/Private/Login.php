<?php include_once("Make/Header.php"); ?>

    <div class="login-body">
        <div class="container">
            <div class="row">
                <div id="tutor-border" class="col-lg-12 col-md-12">
                    <h4>PRIVATE LOGIN</h4>
                    <form action="" method="POST">
                        <div class="form-group">
                            <!-- <label for="username">Full Name</label> -->
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter Full name">
                        </div>
                        <div class="form-group">
                            <!-- <label for="password">Password</label> -->
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                        <p>Forgot password? <a class="forgot-password" href="">Reset here</a></p>
                        <button type="submit" class="btn btn-primary" name="private">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include_once("Make/Footer.php"); ?>



