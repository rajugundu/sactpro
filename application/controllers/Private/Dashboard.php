<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index() {
        if (isset($_POST['private'])) {
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == TRUE) {
                
                $username = $_POST['username'];
                $password = $_POST['password'];
               
                $this->db->select('*');
                $this->db->from('private');
                $this->db->where(array('username' => $username, 'password' => $password));
 
                $query = $this->db->get();

                $user = $query->row();

                if ($user->email) {

                    $this->session->set_flashdata('success', 'SuccessFully logged in');
                    $_SESSION['user_logged'] = TRUE;
                    $_SESSION['username'] = $user->username;

                    redirect("/user/private_profile/", 'refresh');

                } else {
                    $_SESSION['user_logged'] = FALSE;
                    $this->session->set_flashdata("error", 'Please register and try login again');
                    redirect("Login", 'refresh');
                }
            }
        }
        $this->load->view('Private/Login');
    }
}