<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donate extends CI_Controller {
    public function index() {
        if (isset($_POST['f-donate'])) {

            $from_email = "rajudamu08@gmail.com"; 
            $to_email = "sactutorials1@gmail.com"; 
      
            $f_name = $_POST["f-name"];
            $f_phone = $_POST["f-phone"];
            $f_email = $_POST["f-email"];
            $f_message = $_POST["f-message"];
            $f_collect_type = $_POST["f-collect-type"];

            //Load email library 
            $this->load->library('email'); 
      
            $this->email->from($from_email, 'Donation'); 
            $this->email->to($to_email);
            $this->email->subject('Donation'); 
            $this->email->message("Name:".$f_name. "\n" ."Phone:" .$f_phone. "\n" ."Donation-type:" .$f_collect_type. "\n" ."Email:" .$f_email. "\n" ."Message:" .$f_message); 




      
            //Send mail 
            if($this->email->send()) 
            $this->session->set_flashdata("request_sent","Thank you very much, your request has been sent successfully."); 
            else 
            $this->session->set_flashdata("request_sent","Error, please fill and try again"); 
         } 

        $this->load->view('Donate');
    }

}

?>