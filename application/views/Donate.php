<?php
// Define variables for SEO
$pageTitle = 'Donate | SactPro - Find tutor online';
$pageDescription = 'Donate your stuff at sactpro which helps others to learn';
$pageCanonical = 'http://sactpro.com/Donate';

 ?>
<?php include_once("Header.php"); ?>

<div class="about-us">
    <div class="container" style="background-image: url('<?=base_url()?>public/img/donate-bg.png'); color: white !important">
        <div class="row">
            <div class="col-lg-12">
                <h4>SACT PRO - Donate</h4>

            </div>
        </div>
        <div class="row">
<?php if(isset($_SESSION['request_sent'])) { ?>
                <div class="alert alert-success"><?php echo $_SESSION['request_sent']; ?> </div>
            <?php
            }
            ?>
            <div class="col-lg-6">
<p style="color: white !important">Sactpro collects and donates your books to students and people who wants to study and know more about this world. </p>
                <p style="color: white;">Want to be a part of our donation? Please fill the form with your details and mode of donation. You can also drop your donations at sactpro office chaitanyapuri.</p>
<p style="color: white;">For any questions please reach us on 9908836723.</p>
            </div>
            <div class="col-lg-6">

                 <form action="" method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control" id="f-name" name="f-name" placeholder="Your Name" required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="f-phone" name="f-phone" placeholder="Phone Number" required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="f-email" name="f-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="f-collect-type" name="f-collect-type" placeholder="collect-type">
                            <option id="f-collect-type" name="f-collect-type">Pick Up</option>
                            <option id="f-collect-type" name="f-collect-type">Drop at sactpro office</option>
                        </select>
                    </div>
<div class="form-group">
                        <textarea class="form-control" id="f-message" name="f-message" rows="3" placeholder="Your Message"></textarea>
                    </div>
<button style="color: white !important" type="submit" class="btn btn-primary" id="f-donate" name="f-donate">Donate</button>

                </form>


            </div>
        </div>
    </div>
</div>




<?php include_once("Footer.php"); ?>
