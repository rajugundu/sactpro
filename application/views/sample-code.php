                <?php if(isset($_SESSION['error'])) { ?>
                    <div class="alert alert-success"><?php echo $_SESSION['error']; ?> </div>
                <?php 
                }
                ?>


                <?php
                        echo validation_errors("<div class='alert alert-danger'>");
                    ?>


                    <div class="how-it-works">
    <div class="container">
        <h5 class="how-works-title"> How it works</h5>
        <div class="row">
            <div class="col-lg-4">
                <div class="how-works">
                    <img src="<?=base_url()?>public/img/post.png" />
                    <p>Post Your Requirement</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="how-works">
                    <img src="<?=base_url()?>public/img/customize.png" />
                    <p>Get Response</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="how-works">
                    <img src="<?=base_url()?>public/img/compare.png" />
                    <p>Compare & Hire smartly</p>
                </div>
            </div>
        </div>
    </div>
</div>