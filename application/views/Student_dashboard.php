<?php include_once("student_dashboard/Header.php"); ?>

<div clas="dashboard">
	<div class="container-fluid">
		<div class="row">
			<div id="tutor-border" class="col-lg-3">
				<?php include_once("student_dashboard/Sidebar.php"); ?>
			</div>
			<div class="col-lg-9">
				<?php include_once("student_dashboard/Leads.php"); ?>
			</div>
		</div>
	</div>
</div>

<?php include_once("student_dashboard/Footer.php"); ?>



</body>
</html>