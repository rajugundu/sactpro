<div class="leads-title">
    <h5>Current Tutors</h5>
</div>
<div class="leads-desc-title">
    <div class="row">
        <div class="col-lg-3">
            <div class="lead-text">
                <p>Tutor Name</p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="lead-text">
                <p>Tutor Number</p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="lead-text">
                <p>Subject</p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="lead-text">
                <p>Timing</p>
            </div>
        </div>
    </div>
</div>
<div class="leads">
    <?php if ($tutor_name == '' ) { ?>
            <p> No Tutors Assigned </p>
        <?php } else { ?>
    <div class="row">
        <div class="col-lg-3">
            <div class="profile-img">
                <p><?php echo $tutor_name; ?></p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="phone">
            <p><?php echo $tutor_number ?></p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="subject">
                <p><?php echo $subject; ?></p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="time">
            <p><?php echo $timing; ?></p>
            </div>
        </div>
    </div>
</div>

<?php } ?>
