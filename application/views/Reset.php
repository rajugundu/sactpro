<?php include_once("Header.php"); ?>


    <div class="login-body">
        <div class="container">
            <div class="row">
                <div id="tutor-border" class="col-lg-12 col-md-12">
                    <?php if(isset($_SESSION['error'])) { ?>
                        <div class="alert alert-success"><?php echo $_SESSION['error']; ?> </div>
                    <?php 
                    }
                    ?>
                    <h4>RESET YOUR PASSWORD</h4>
                    <form action="" method="POST">
                        <div class="form-group">
                            <input type="text" class="form-control" id="r_username" name="r_username" placeholder="Enter Username or Email">
                        </div>
                        <button type="submit" class="btn btn-primary" name="reset">Reset Password</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include_once("Footer.php"); ?>



