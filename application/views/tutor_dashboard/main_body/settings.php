<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Profile</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <!-- <form action="" method="POST"> -->
        <?php echo form_open_multipart('Settings/do_multi_upload');?>

            <div class="form-group">
                <!-- <label for="email">Email address</label> -->
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Update Subject (Use , for more subjects)" required autofocus>
                <!-- <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
            </div>
            <div class="form-group">
                <!-- <label for="email">Email address</label> -->
                <input type="text" class="form-control" id="class" name="class" placeholder="Update Class" required autofocus>
                <!-- <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
            </div>
            <div class="form-group">

                        <select class="form-control" id="timing" name="timing" placeholder="Prefered Timing">
                            <option id="timing" name="timing" selected="true" disabled="disabled">Prefered Timing</option>
                            <option id="timing" name="timing">Morning (8:am-12pm)</option>
                            <option id="timing" name="timing">Afternoon (1:pm-4pm)</option>
                            <option id="timing" name="timing">Evening (5:pm-7pm)</option>
                            <option id="timing" name="timing">Night (8:pm-10pm)</option>
                        </select>
                    </div>
<div class="form-group">

                <input type="text" class="form-control" id="city" name="city" placeholder="Update City" required autofocus>

            </div>


            <div class="form-group">
                <label for="ifsc_code">Upload your Profile Photo</label>
                <input type="file" name="profile_pic[]" size="200000"required autofocus />
            </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="submit" class="btn btn-primary" name="update">Update</button> -->
        <input type="submit" class="btn btn-primary" value="Update" name="submit_form"/>

      </div>
      </form>

    </div>
  </div>
</div>



<div id="profile" class="tab-pane fade">
    <div class="leads-title">
        <h5>Profile</h5>
    </div>
    <?php $tutor_profile = $this->db->get_where('users')->result();
        foreach ($tutor_profile as $tutor_details ) {
            if ($_SESSION['username'] == $tutor_details->username) { ?>
                <div class="container">
                    <p><strong>Name: </strong><?php echo $tutor_details->username; ?></p>
                    <p><strong>Email: </strong><?php echo $tutor_details->email; ?></p>
                    <p><strong>Phone: </strong><?php echo $tutor_details->number; ?></p>
                    <p><strong>Class: </strong><?php echo $tutor_details->class; ?></p>
                    <p><strong>Subject: </strong><?php echo $tutor_details->subject; ?></p>
                    <p><strong>Board: </strong><?php echo $tutor_details->board; ?></p>
                    <p><strong>Timings: </strong><?php echo $tutor_details->timing; ?></p>
                    <p><strong>Address: </strong><?php echo $tutor_details->address; ?></p>
                    <p><strong>City: </strong><?php echo $tutor_details->city; ?></p>
                    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Edit Profile</button> -->
                </div>
            <?php }
        } ?>

</div>
