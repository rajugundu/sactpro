<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="<?=base_url()?>public/img/favicon.png"> 

    <link rel="stylesheet" href="<?=base_url()?>public/css/style.css" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url()?>public/bootstrap/css//bootstrap.min.css" crossorigin="anonymous">

    <title>SAC | Find Tutor Online</title>
  </head>
  <body>
  <div class="top-header">
    <div class="container-fluid">
      <div id="header" class="row">
        <div class="col-lg-9">
          <div class="main-logo">
            <a class="dashboard-logo" href="http://localhost:8888/SAC/index.php/user/student_profile/"><img src="<?=base_url()?>public/img/main_logo.png" /></a>
          </div>
        </div>
        <div class="col-lg-3">
          <div id="navigation" class="nav nav-tabs">
            <!-- <div id="margin25px"> -->
              <p id="margin25px"><a class="border-btn" href="<?php echo base_url(); ?>login/logout">Logout</a><p>
            <!-- </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>


