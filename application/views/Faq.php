<?php
// Define variables for SEO
$pageTitle = 'Faq | SactPro - Find tutor online';
$pageDescription = 'Sactpro frequently asked questions';
$pageCanonical = 'http://sactpro.com/faq';

 ?>
<?php include_once("Header.php"); ?>


<div class="faq">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="faq">
                    <div class="panel-group" id="accordion">
                        <div class="faqHeader">Tutor FAQ's</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Is account registration required?</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Account registration is required. Sactpro will collect all your details and provide leads according to the requirements.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">I just registered as tutor. What should I do next?</a>
                                </h4>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Great! You have taken the first step towards growing your career as a home tutor. Next step is to complete your profile and you need to upgrade to membership. 
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Can I change my subjects and classes that i want to teach after registration?</a>
                                </h4>
                            </div>
                            <div id="collapseEleven" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yes. You can update your profile at update profile section after logining into your account.
                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Student FAQ's</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">How do I use sactpro.com to find a prefect tutor?</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Simply register as student by providing your requirement and personal details. Our team will get in touch with you soon.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Will i get a demo class before i accepting the tutor?</a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yes, you do get a demo class after assigning a tutor by sactpro. You can accept the tutor only when you find a good fit.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once("Footer.php"); ?>
