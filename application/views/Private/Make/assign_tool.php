<div class="leads-title">
    <h5>Tutor Assign tool</h5>
</div>
<div class="leads-desc-title">
    <div class="row">
        <div class="col-lg-6">
            <div class="lead-text">
                <p>Tutor Name</p>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="lead-text">
                <p>Assign a Student</p>
            </div>
        </div>
    </div> 
</div>


<div class="leads"> 
    <div class="row">
        <div class="col-lg-3">
            <div class="address">
                <form action="" method="post">
                    <div class="form-group">
                        <select class="form-control" id="t_name" name="t_name">
                            <?php foreach($t_data as $T_value) { ?>
                                <option id="t_name" name="t_name"><?php echo $T_value->username; ?></option>
                            <?php } ?>
                        </select>
                    </div> 
                 </div>
            </div>

        <div class="col-lg-3">
            <div class="address">
                <div class="form-group">
                     <select class="form-control" id="student_name" name="student_name">
                        <?php foreach($s_data as $S_value) { ?>
                            <option id="student_name" name="student_name"><?php echo $S_value->username; ?></option>
                        <?php } ?>
                    </select>
                </div> 
            </div>
        </div>
        <div class="col-lg-3">
            <div class="address">
                <div class="form-group">
                     <select class="form-control" id="student_number" name="student_number">
                        <?php foreach($s_data as $S_value) { ?>
                            <option id="student_number" name="student_number"><?php echo $S_value->number; ?></option>
                        <?php } ?>
                    </select>
                </div> 
            </div>
        </div>

        <div class="col-lg-3">
            <div class="address">
                <div class="form-group">
                    <select class="form-control" id="student_address" name="student_address">
                        <?php foreach($s_data as $S_value) { ?>
                            <option id="student_address" name="student_address"><?php echo $S_value->address; ?></option>
                        <?php } ?>
                    </select>
                </div> 
                <button type="submit" class="btn btn-primary" name="update-assign" name="update-assign">Assign</button>
                </form>    
            </div>
        </div>
    </div>  
</div>

