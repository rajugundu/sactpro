<?php
// Define variables for SEO
$pageTitle = 'SactPro | Find Tutor Online';
$pageDescription = 'Finding tutor online made easy by connecting Tutors and Students on one platform.';
$pageCanonical = 'http://sactpro.com/';
 ?>
<?php include_once("Header.php"); ?>
<?php include_once("Body.php"); ?>
<?php include_once("Footer.php"); ?>
