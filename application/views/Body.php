
<div id="NewmyModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #233759; color: white; border-radius: 0;">
        <button style="color: white !important;" type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="text-align: center !important; padding: 0; margin: 20px 0 0 0;">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="col_withborder">
                <h4 style="margin-bottom: 20px;">Do you want a Tutor?</h4>
                <p>SactPro helps you to find a right tutor. Please register a student below</p>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="col_noborder" style="padding: 20px 15px;">
                <h4 style="margin-bottom: 20px;">Want to be a Tutor?</h4>
                <p>Please register as tutor below. SactPro has over 500+ tuitions available.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer" style="text-align: center; border-top: 0; padding: 0; margin: 0 0 20px 0;">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="col_withborder">
                <a class="btn btn-primary" style="background: none !important; color: #233759 !important; border: 2px solid #233759; border-radius: 2px; text-transform: initial;" href="<?=base_url()?>register">Register as Student</a>
              </div>
            </div>
            <div class="col-lg-6">
              <div style="padding: 20px 40px 20px 20px;">
                <a class="btn btn-primary" style="background: none !important; color: #233759 !important; border: 2px solid #233759; border-radius: 2px; text-transform: initial; padding: 5px 22px;" href="<?=base_url()?>register">Register as Tutor</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="main-bg" style="background-image: url(<?=base_url()?>public/img/main-bg-one.png) !important">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h5>Welcome to</h5>
                <h2>SactPro</h5>
                <p>The leading provider of qualified and expert tutors in India.</p>
            </div>
        </div>
    </div>
</div>

<div id="counter" class="counter">
    <div class="main_counter_area">
        <div class="overlay p-y-3">
            <div class="container">
                <div class="row">
                    <!-- <div class="main_counter_content text-center white-text wow fadeInUp"> -->
                        <div class="col-md-3 col-lg-3">
                            <div class="single_counter p-y-2 m-t-1">
                                <i class="fa fa-book m-b-1"></i>
                                <h2 class="statistic-counter">210</h2>
                                <p>Subjects</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="single_counter p-y-2 m-t-1">
                                <i class="fa fa-check m-b-1"></i>
                                <h2 class="statistic-counter">2500</h2>
                                <p>Registerd Tutors</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="single_counter p-y-2 m-t-1">
                                <i class="fa fa-graduation-cap m-b-1"></i>
                                <h2 class="statistic-counter">3200</h2>
                                <p>Available requirements</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="single_counter p-y-2 m-t-1">
                                <i class="fa fa-location-arrow m-b-1"></i>
                                <h2 class="statistic-counter">614</h2>
                                <p>Locations</p>
                            </div>
                        </div>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="how-it-works">
    <div class="container">
        <h5 class="how-works-title"> How it works</h5>
        <div class="row">
            <div class="col-lg-4">
                <div class="how-works">
                    <img src="<?=base_url()?>public/img/post.svg" />
                    <p>Post Your Requirement</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="how-works">
                    <img src="<?=base_url()?>public/img/customize.svg" />
                    <p>Get Response</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="how-works">
                    <img src="<?=base_url()?>public/img/compare.svg" />
                    <p>Compare & Hire smartly</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="register-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="register-area-text">
                    <h6>Find Tutors in any category</h6>
                    <p>Over 25 Students have already found the right Tutor through us!</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="register-button-area">
                    <button class="button" onclick="location.href='register'">Request a Tutor</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Teachers working model -->

<div class="how-it-works">
    <div class="container">
        <h5 class="how-works-title"> How it works for tutors</h5>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="nav-item">
                            <a class="nav-link active" id="register-icon" data-toggle="tab" href="#panel1" role="tab"><img src="<?=base_url()?>public/img/icons/register.svg" /></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="update-icon" data-toggle="tab" href="#panel2" role="tab"><img src="<?=base_url()?>public/img/icons/update.svg" /></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="leads-icon" data-toggle="tab" href="#panel3" role="tab"><img src="<?=base_url()?>public/img/icons/leads.svg" /></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="demo-icon" data-toggle="tab" href="#panel4" role="tab"><img src="<?=base_url()?>public/img/icons/demo.svg" /></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="confirm-icon" data-toggle="tab" href="#panel5" role="tab"><img src="<?=base_url()?>public/img/icons/confirm.svg" /></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div id="card" class="tab-content card">
                        <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                            <h5>SignUp</h5>
                            <p>Register as tutor using your email, phone number and create a password.<p>
                            <div id="navigation" class="nav nav-tabs">
                                <p class="navigator"><a class="border-btn" href="<?=base_url()?>register">REGISTER</a></p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="panel2" role="tabpanel">
                            <h5>Update Profile</h5>
                            <p>Update your profile with your teaching skills, experience, subjects. Choose a plan and make a payment online. </p>
                            <div id="navigation" class="nav nav-tabs">
                                <p class="navigator"><a class="noborder-btn" href="<?=base_url()?>login">LOGIN</a></p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="panel3" role="tabpanel">
                            <h5>Get Leads</h5>
                            <p>After making a payment, our team will assess your profile for matching students near to your locailty.</p>
                        </div>
                        <div class="tab-pane fade" id="panel4" role="tabpanel">
                            <h5>Make a demo</h5>
                            <p>Once a student is assigned you will get a notification from SAC to your phone and email. Then you can book a demo class with the student.</p>
                        </div>
                        <div class="tab-pane fade" id="panel5" role="tabpanel">
                            <h5>Confirm</h5>
                            <p>If everything is goes well, you are ready for teaching</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="register-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="register-area-text">
                    <h6>Reach out to more students</h6>
                    <p>Join as Tutors and Trainers on SAC and get enquiries!</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="register-button-area">
                    <button class="button" onclick="location.href='register'">Create your own profile</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- about area -->

<div class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="about-text-area-border">
                    <h5>Find tutors for all classes</h5>
                    <p>KG-X class, CBSE, ICSE, IGCSE, Engineering, EAMCET-IIT-JEE , CPT-IPCC, BCOM-BAF, BFM-BBI, BBM-BMS, MBA-BBA, MBBS-BDS, Spoken English, Competitive-Exams, GRE-GMAT, TOEFL-IELTS, Computers Tuitions and many more.</p>
                    <div class="register-button-area">
                        <!-- <button class="button">Go to Schools page</button> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="about-text-area-border">
                    <h5>Find students near by</h5>
                    <p>SAC Tuition always look forward for Tutors and students convenience. Tutors can find students near by locality. </p>
                    <div class="register-button-area">
                        <!-- <button class="button">Go to worksheets</button> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="about-text-area">
                    <h5>Smart Hire</h5>
                    <p>Students and Parents can hire tutors after a demo class. This helps tutors to understand better about, what students are looking for and also helps students to hire a best tutor.</p>
                    <div class="register-button-area">
                        <!-- <button class="button">Check it now</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Write to us -->

<div class="write">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h5>Write to us.</h5>
                <p>For all questions, queries on SAC Tuition or Feedback about our work. Please write us a message. We will get back to you as soon as possible.</p>
            </div>
            <div class="col-lg-8">
                <div class="sent-message" style="text-align: center;"><?php echo $this->session->flashdata('email_sent'); ?> </div>
                <form action="" method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control" id="f-name" name="f-name" placeholder="Your Name" required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="f-phone" name="f-phone" placeholder="Phone Number">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="f-message" name="f-message" rows="3" placeholder="Your Message" required autofocus></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary" id="f-submit" name="f-submit">Send</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="testimonial">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-center m-auto">
			<h3 style="text-align: center;">What people are saying about sactpro:</h3>
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Carousel indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
				</ol>
				<!-- Wrapper for carousel items -->
				<div class="carousel-inner">
					<div class="item carousel-item active">
						<div class="img-box"><img src="<?=base_url()?>public/img/testimonials/jd-abhishek.png" alt=""></div>
						<p class="testimonial">Learning is super fun with SactPro as they have a wide range of professionals who understand the students and train them to excel in this competitive arena</p>
						<p class="overview"><b>Abishek JD</b>, Student</p>
					</div>
					<div class="item carousel-item">
						<div class="img-box"><img src="public/img/testimonials/konchada-srinivas.png" alt=""></div>
						<p class="testimonial">I have worked with sactpro for two years. They are providing excellent service for both Tutors and Students. I'm glad to travel with them.</p>
						<p class="overview"><b>Srinivas Konchada</b>, Tutor</p>
					</div>
					<div class="item carousel-item">
						<div class="img-box"><img src="public/img/testimonials/srinivas-yadav.png" alt=""></div>
						<p class="testimonial">SactPro provides tutors as per your Requirement. Your child is tutored from convenience of your home with experienced tutors</p>
						<p class="overview"><b>Srinivas yadav</b>, Caligraphy tutor</p>
					</div>
          <div class="item carousel-item">
            <div class="img-box"><img src="public/img/testimonials/hema-sai.png" alt=""></div>
            <p class="testimonial">SactPro guarantee better grades and improve results, if you avail services</p>
            <p class="overview"><b>Hema Sai</b>, Tutor</p>
          </div>
				</div>

				<!-- Carousel controls -->
				<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>
		</div>
	</div>
</div>
</div>
