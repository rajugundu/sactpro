<div class="leads-title">
    <h5>Students Registered</h5>
</div>
<div class="leads-desc-title">
    <div class="row">
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Student Name</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Email</p>
            </div>
        </div>
        <div class="col-lg-1">
            <div class="lead-text">
                <p>Mobile Number</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Subject</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Timings</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Address</p>
            </div>
        </div>
        <div class="col-lg-1">
            <div class="lead-text">
                <p>Assigned</p>
            </div>
        </div>
    </div> 
</div>


<?php 
foreach($s_data as $S_value) {
?>

<div class="leads"> 
    <div class="row">
        <div class="col-lg-2">
            <div class="profile-img">
                <option id="id" name="id"><?php echo $S_value->id; ?></option>
                <p><?php echo $S_value->username; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="email" style="text-transform: lowercase;">
                <p><?php echo $S_value->email; ?></p>
            </div>
        </div>
        <div class="col-lg-1">
            <div class="phone">
            <p><?php echo $S_value->number; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="subject">
                <p><?php echo $S_value->subject; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="time">
            <p><?php echo $S_value->timing; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="address">
                <p><?php echo $S_value->address; ?></p>
            </div>
        </div>
        <div class="col-lg-1">
            <div class="assigned">
                <p><?php echo $S_value->assigned; ?></p>
            </div>
        </div>
    </div>  
</div>

<?php
}
?>





<div class="leads-title">
    <h5>Tutors Registered</h5>
</div>
<div class="leads-desc-title">
    <div class="row">
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Tutor Name</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Email</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Mobile Number</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Subject</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Address</p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="lead-text">
                <p>Assigned Student</p>
            </div>
        </div>
    </div> 
</div>


<?php 
foreach($t_data as $T_value) {
?>

<div class="leads"> 
    <div class="row">
        <div class="col-lg-2">
            <div class="tutor-name">
                <p><?php echo $T_value->username; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="email" style="text-transform: lowercase;">
                <p><?php echo $T_value->email; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="phone">
            <p><?php echo $T_value->number; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="subject">
                <p><?php echo $T_value->subject; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="time">
                <p><?php echo $T_value->address; ?></p>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="address">
                <p><?php echo $T_value->student_name; ?></p>
            </div>
        </div>
    </div>  
</div>

<?php
}
?>



<?php include_once("assign_tool.php"); ?>
