<?php
// Define variables for SEO
$pageTitle = 'Login | SactPro - Find tutor online';
$pageDescription = 'Login to your Sactpro account and find leads.';
$pageCanonical = 'http://sactpro.com/login';

 ?>
<?php include_once("Header.php"); ?>
    <div class="login-body">
        <div class="container">
            <div class="row">
                <div id="tutor-border" class="col-lg-6 col-md-12">
                    <?php if(isset($_SESSION['error'])) { ?>
                        <div class="alert alert-success"><?php echo $_SESSION['error']; ?> </div>
                    <?php
                    }
                    ?>
                    <h4>TUTOR LOGIN</h4>
                    <form action="" method="POST">
                        <div class="form-group">
                            <!-- <label for="username">Full Name</label> -->
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter Full name" required autofocus>
                        </div>
                        <div class="form-group">
                            <!-- <label for="password">Password</label> -->
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required autofocus>
                        </div>
                        <p>Forgot password? <a class="forgot-password" href="">Reset here</a></p>
                        <button type="submit" class="btn btn-primary" name="login">Login</button>
                    </form>
                </div>
                <div class="col-lg-6 col-md-12">
                    <?php
                        echo validation_errors();
                    ?>
                    <h4>STUDENT LOGIN</h4>
                    <form action="" method="POST">
                        <div class="form-group">
                            <!-- <label for="username">Full Name</label> -->
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter Full name" required autofocus>
                        </div>
                        <div class="form-group">
                            <!-- <label for="password">Password</label> -->
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required autofocus>
                        </div>
                        <p>Forgot password? <a class="forgot-password" href="">Reset here</a></p>
                        <button type="submit" class="btn btn-primary" name="login_student">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php include_once("Footer.php"); ?>
