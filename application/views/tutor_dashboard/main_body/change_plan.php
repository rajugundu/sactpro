

<?php if(isset($_SESSION['paid'])) { ?>
    <div class="alert alert-success"><?php echo $_SESSION['paid']; ?> </div>
<?php
}
?>
<div id="change_plan" class="tab-pane fade">
  <div class="leads-title">
      <h5>Pricing</h5>
  </div>
  <div class="container">
    <div class='row'>
      <div class="col-lg-4">
        <div class="columns">
          <ul class="price">
            <li class="header">Basic</li>
            <li class="grey">₹500</li>
            <li>Can get upto 5 requirements</li>
            <li>SMS & Email Support</li>
            <li>---</li>
            <li>Membership valid for 3 months</li>
          </ul>
        </div>
        <form action="" method="POST">
          <script
              src="https://checkout.razorpay.com/v1/checkout.js"
              data-key='rzp_live_P299fve0lF4asA'
              data-amount="50000"
              data-buttontext="Pay"
              data-name="SactPro"
              data-description="Tutor Subscription Fees"
              data-image="<?=base_url()?>public/img/main_logo.png"
              data-prefill.name=<?php echo $username ?>
              data-prefill.contact=<?php echo $number ?>
              data-prefill.email=<?php echo $email ?>
              data-theme.color="#233759">
          </script>
        <input type="hidden" value="Hidden Element" name="hidden">
        </form>

      </div>
      <div class="col-lg-4">
        <div class="columns">
          <ul class="price">
            <li class="header">Pro</li>
            <li class="grey">₹1000</li>
            <li>Can get upto 10 requirements</li>
            <li>SMS & Email Support</li>
            <li>Support from Sact team</li>
            <li>Membership valid for 3 months</li>
          </ul>
        </div>
        <?php include_once("Plans/Pro.php"); ?>
      </div>
      <div class="col-lg-4">
        <div class="columns">
          <ul class="price">
            <li class="header">Premium</li>
            <li class="grey">₹1500</li>
            <li>Can get upto 15 requirements</li>
            <li>Call, SMS & Email Support</li>
            <li>Support from Sact team</li>
            <li>Membership valid for 3 months</li>
          </ul>
        </div>
        <?php include_once("Plans/Premium.php"); ?>
      </div>
    </div>
  </div>
  <div class="leads-title">
      <h6>*Coins will be credited to your dashboard in 10 Minutes.</h6>
  </div>
</div>
