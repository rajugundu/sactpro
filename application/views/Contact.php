<?php
// Define variables for SEO
$pageTitle = 'Contact Us | SactPro - Find tutor online';
$pageDescription = 'Contact us to help answer all your queries faster.';
$pageCanonical = 'http://sactpro.com/contact';
 ?>
<?php include_once("Header.php"); ?>
<div class="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4>SACT PRO - Contact us</h4>
                <h5>CONTACT</h5>
                <p><strong>Phone:</strong> 8142750189, 9182374217</p>
                <p><strong>Email:</strong> sactutorials1@gmail.com</p>
                <h5>VISIT US</h5>
                <p>Mon - Fri: 10 am to 5 pm</p>
                <h5>ADDRESS</h5>
                <p>1st Floor, H-No:18-83, F:No: 102 Guru Nivas, Beside Centro, Chaitanyapuri, Dilsuknagar, Hyderabad, Telangana 500060</p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2860.964765816423!2d78.5275740155811!3d17.367792142496302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9b5da762b507e910!2sPolycet+%2Fceep+coaching!5e0!3m2!1sen!2sin!4v1522776503082" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>







<?php include_once("Footer.php"); ?>
