<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __constuct() {
        parent::__constuct();
        if(isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata('error', 'Please login first');
            redirect("login", 'refresh');
        }
    }

    // Tutor Profile Dashboard
    public function tutor_profile() {
        if($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata('error', 'Please login first');
            redirect("login", 'refresh');
        }
        $username = $_SESSION['username'];
        $tutor = $this->db->get_where('users', array('username' => $username));
        $results = $tutor->result();
        $array = $this->db->get_where('users', array('username' => $username))->result();

      	foreach ($array as $profile_valid) {
      		if($profile_valid->subject !== "") {
            // Creating pagination
            $data_array = json_decode(json_encode($profile_valid), true);

            $config['base_url'] = base_url('/user/tutor_profile');
            $config['total_rows'] = $this->db->get('student_registration')->num_rows();
            $config['per_page'] = 7;
            $config['num_links'] = 10;
            $config['uri_segment'] = 3;
            //styling
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['next_tag_open'] = '<li class="pg-next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="pg-prev">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            $config['records'] = $this->db->select('id, username, number, city, class, subject')->get('student_registration', $config['per_page'], $this->uri->segment(3));
            $config['data'] = $data_array;
            $this->pagination->initialize($config);
            $this->load->view('Tutor_dashboard', $config);
      		} else {
            $this->load->view('Multi', $profile_valid);
      		}
      	}

        // $this->load->view('Tutor_dashboard', $array);
    }

    // Student Profile Dashboard

    public function student_profile() {
        if($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata('error', 'Please login first');
            redirect("login", 'refresh');
        }
        $username = $_SESSION['username'];
        $student = $this->db->get_where('student_registration', array('username' => $username));
        $results = $student->row();
        $array = json_decode(json_encode($results), true);
        $this->load->view('Student_dashboard', $results);

    }

    // Private Profile Dashboard

    public function private_profile() {
        if($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata('error', 'Please login first');
            redirect("login", 'refresh');
        }
        $student = $this->db->get('student_registration')->result();
        $tutor = $this->db->get('users')->result();
        $this->load->view('Private/Build', array('s_data' => $student,'t_data' => $tutor));

        // Updating assigned users

        if (isset($_POST['update-assign'])) {

            $this->form_validation->set_rules('student_name', 'student_name', 'required');

            if ($this->form_validation->run() == TRUE) {

                $data = array(
                    'student_name'      => $_POST['student_name'],
                    'student_number'      => $_POST['student_number'],
                    'student_address'      => $_POST['student_address']

                );
                $username = $_POST["t_name"];
                $this->db->where('username', $username);
                $this->db->update('users', $data);
                $this->session->set_flashdata('success', 'Your account has been created');

            }
            else {
                $this->session->set_flashdata('error', 'Error');
            }
        }

    }


}
?>
