<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Build_up extends CI_Controller {
    public function index() {

        if (isset($_POST['f-submit'])) {

            $from_email = "rajudamu08@gmail.com";
            $to_email = "sactutorials1@gmail.com";

            $f_name = $_POST["f-name"];
            $f_phone = $_POST["f-phone"];
            $f_message = $_POST["f-message"];

            //Load email library
            $this->load->library('email');

            $this->email->from($from_email, 'Enquiry');
            $this->email->to($to_email);
            $this->email->subject('New Enquiry from website');
            $this->email->message("Name:".$f_name. "Phone:" .$f_phone. "Message:" .$f_message);

            //Send mail
            if($this->email->send())
            $this->session->set_flashdata("email_sent","Email sent successfully.");
            else
            $this->session->set_flashdata("email_sent","Error in sending Email.");
         } 

        $this->load->view('Build_up');
    }

}

?>
