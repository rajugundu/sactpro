<div class="profile-image">
    <a href="#">
      <?php if ($data['photo'] == "") { ?>
        <img src="http://sactpro.com/public/img/sample-profile.png" />
      <?php  } else { ?>
          <img src="http://sactpro.com/<?php echo $data['photo']; ?>" />

        <?php } ?>
    </a>
    <div class="profile-details">
        <h6><?php echo $_SESSION['username']; ?></h6>
        <p><strong>Tutor ID: </strong><?php echo $data['id']; ?></p>
    </div>
    <div class="setting-info">
        <ul class="sidebar-list">
            <li class="active"><a data-toggle="tab" href="#dashboard">Dashboard</a></li>
            <li><a data-toggle="tab" href="#profile">Profile</a></li>
            <li><a data-toggle="tab" href="#subscribed">My Requirments</a></li>
            <li><a data-toggle="tab" href="#change_plan">Buy Credits</a></li>
            <li><a data-toggle="tab" href="#support">Support</a></li>
        </ul>
    </div>
</div>
<p class="small-text">Powered By <a href="https://stripedigital.in">Stripe Digital</a></p>
