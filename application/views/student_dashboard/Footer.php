<div class="copyright"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="copyright-text">
                    <p>Copyright 2018 SAC Tutorials. All right reserved.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="copyright-links">
                    <a href="http://localhost:8888/SAC/privacy">Privacy Policy</a>
                    <a href="">Terms & Conditions</a>
                </div>
            </div>
        </div>
    </div>
</div>