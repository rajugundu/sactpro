<div id="subscribed" class="tab-pane fade">
    <div class="leads-title">
        <h5>Purchased Requirments</h5>
    </div>
    <div class="leads-desc-title">
        <div class="row">
          <div class="col-lg-2">
              <div class="lead-text">
                  <p>Name</p>
              </div>
          </div>
            <div class="col-lg-2">
                <div class="lead-text">
                    <p>Phone</p>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="lead-text">
                    <p>Class / Board</p>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="lead-text">
                    <p>Subject</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="lead-text">
                    <p>Address</p>
                </div>
            </div>
        </div>
    </div>
    <?php $student_details = $this->db->get_where('coins_subscribe', array('user_username' => $_SESSION['username']))->result();
      foreach ($student_details as $student_views ) {
         $job_id = $student_views->id;

         if ($student_views->user_username !== $_SESSION['username']) {
           ?>

           <div class="leads-desc-title">
             <div class="row">
                 <div class="col-lg-12">
                     <div class="profile-img">
                         <p>No subscribed Requirments</p>
                     </div>
                 </div>
             </div>
           </div>

         <?php } else { ?>

           <div class="leads">
             <div class="row">
                 <div class="col-lg-2">
                     <div class="profile-img">
                         <p><?php echo $student_views->student_username; ?></p>
                     </div>
                 </div>
                 <div class="col-lg-2">
                     <div class="phone">
                       <p><?php echo $student_views->student_number ?></p>
                     </div>
                 </div>
                 <div class="col-lg-2">
                     <div class="time">
                         <p><?php echo $student_views->student_class; ?></p>
                     </div>
                 </div>
                 <div class="col-lg-2">
                     <div class="lead-text">
                       <p><?php echo $student_views->student_subject; ?></p>
                     </div>
                 </div>
                 <div class="col-lg-4">
                     <div class="profile-img">
                         <p><?php echo $student_views->student_address; ?></p>
                         <p><?php echo $student_views->student_city; ?></p>
                     </div>
                 </div>
             </div>
           </div>

         <?php }
        ?>


  <?php } ?>


</div>
