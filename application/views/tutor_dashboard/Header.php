<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="City Home Tuition, Home Tuition, Tutor Jobs, Private Tutors, Tutor, Tutors, Sactpro, Sac, Tuitions, Students, Computers Tuitions, find tutors nearby, hire best tutor">
    <meta name="description" content="Sactpro. Finding a tutor made easy through online by connecting Tutors and Students on one platform.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Stripe Digital">
    <link rel="icon" href="<?=base_url()?>public/img/favicon.png">

    <link rel="stylesheet" href="<?=base_url()?>public/css/style.css" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url()?>public/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">

    <title>SactPro | Find Tutor Online</title>
  </head>
  <body>
  <div class="top-header">
    <div class="container-fluid">
      <div id="header" class="row">
        <div class="col-lg-9">
          <div class="main-logo">
            <a class="dashboard-logo" href="<?=base_url()?>/user/tutor_profile/"><img src="<?=base_url()?>public/img/main_logo.png" /></a>
          </div>
        </div>
        <div class="col-lg-3">
          <div id="navigation" class="nav nav-tabs">
            <?php  ?>
            <!-- <div id="margin25px"> -->
              <p id="margin25px">
                <a style="margin-right: 20px;" class='credits'><strong>Credits:</strong> <?php echo $data['paid']; ?></a>
                <a class="border-btn" href="<?php echo base_url(); ?>login/logout">Logout</a>
              <p>
            <!-- </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
