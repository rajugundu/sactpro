<div class="profile-image">
    <a href="#"><img src="<?=base_url()?>public/img/sample-profile.png" /></a>
    <div class="profile-details">
        <h6><?php echo $_SESSION['username']; ?></h6>

    </div>
    <div class="setting-info">
        <ul class="sidebar-list">
            <li><a href="">Settings</a></li>
            <li><a href="">Change Password</a></li>
            <li><a href="">Change Plan</a></li>
            <li><a href="">Support</a></li>
        </ul>
    </div>
</div>
<p class="small-text">Powered By <a href="https://stripedigital.in">Stripe Digital</a></p>
