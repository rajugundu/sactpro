<div id="support" class="tab-pane fade">
    <div class="leads-title">
        <h5>Support</h5>
        <p>Please contact us for any support related or questions related queries, send us a message: </p>
    </div>
    <div class="sent-message" style="text-align: center;"><?php echo $this->session->flashdata('email_sent'); ?> </div>
    <div class="container">
        <form action="" method="POST">
            <div class="form-group">
                <input type="text" class="form-control" id="f-name" name="f-name" placeholder="Your Name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="f-phone" name="f-phone" placeholder="Phone Number">
            </div>
            <div class="form-group">
                <textarea class="form-control" id="f-message" name="f-message" rows="3" placeholder="Your Message"></textarea>
            </div>
            <button type="submit" class="btn btn-primary" id="f-submit" name="f-submit">Send</button>
        </form>
    </div>
</div>
