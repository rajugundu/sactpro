<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
        if (isset($_POST['register'])) {
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('number', 'Number', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');


            $email = $_POST["email"];
						$username = $_POST["username"];
						$phone = $_POST["number"];

            $this->email->from('sactutorials1@gmail.com', 'Registration');
            $this->email->to($email);
            $this->email->cc('sactutorials1@gmail.com');
            $this->email->subject('Thanks For Registering with SAC Tutorials.');
            $this->email->message('Thanks For Registering as Tutor with SAC Tutorials.
						Here is your info:
						Username:'.$username.',
						Phone: '.$phone.',
						Email: '.$email.'.
						Please login to your account and fill account details to get leads. http://sactpro.com');

						$paid = "0";

            if ($this->form_validation->run() == TRUE) {
                $data = array (
                    'username' => $_POST['username'],
                    'number' => $_POST['number'],
                    'email' => $_POST['email'],
                    'password' => $_POST['password'],
										'paid' => $paid,

                );
                $this->email->send();
                $this->db->insert('users', $data);
                $this->session->set_flashdata('success', 'Your account has been created. Please check your email to verify your account.');
            }
            else {
                $this->session->set_flashdata('error', 'Email id already existed');
            }
        }

        if (isset($_POST['student_register'])) {

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('number', 'Number', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
						$this->form_validation->set_rules('class', 'Class', 'required');
            $this->form_validation->set_rules('subject', 'Subject', 'required');
            $this->form_validation->set_rules('timing', 'Timing', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');

            $email = $_POST["email"];

            $this->email->from('sactutorials1@gmail.com', 'Registration');
            $this->email->to($email);
            $this->email->cc('sactutorials1@gmail.com');
            $this->email->subject('Thanks For Registering as Student with SAC Tutorials.');
            $this->email->message('Thanks For Registering as student with SAC Tutorials. We"ll send tutor leads shortly');


            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'username' => $_POST['username'],
                    'number' => $_POST['number'],
                    'email' => $_POST['email'],
                    'password' => $_POST['password'],
										'class' => $_POST['class'],
                    'subject' => $_POST['subject'],
                    'timing' => $_POST['timing'],
                    'city' => $_POST['city'],
                    'address' => $_POST['address'],

                );
                $this->email->send();

                $this->db->insert('student_registration', $data);

                $this->session->set_flashdata('success', 'Your account has been created. Please check your email to verify your account.');
            }
        }

        $this->load->view('Register');
    }

}
