<?php
// Define variables for SEO
$pageTitle = 'Terms of use | SactPro - Find tutor online';
$pageDescription = 'Terms and conditions for using sactpro.com';
$pageCanonical = 'http://sactpro.com/terms';

 ?>

<?php include_once("Header.php"); ?>


<div class="privacy">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4>SACT PRO - Terms & Conditions</h4>
                <h5>Last Updated: May 6, 2018</h5>
                <p>1. Your status must be an Indian citizen. You understand that Sactpro is a home tutoring marketplace and will facilitate through its platform and other resources finding for you such students as who want home tuition in your area of expertise.</p>
                <p>2. A Tutor warrants that he is of at least Twenty years of age, and having necessary qualifications experience to give tutoring in the Categories/Syllabus and subjects specified by the Tutor on his/her profile.</p>
                <p>3. You are responsible for keeping your password secret and secure. We also provide a reset option in case of forgot password.</p>
                <p>4. As a tutor providing home tuitions, you agree to comply with all applicable laws, exercise the same degree of professional competence, care, skill, d	iligence and prudence as is normally exercised by professionals in the home- tutoring field.</p>
                <p>5. You may receive information proprietary to Sactpro. You understand and agree that confidential information so received will only be used by you for the benefit of Sactpro and for the purpose of providing tutoring.</p>
                <p>6. You indemnify Sactpro from and against any and all claims, causes of action, and liabilities which arise directly from breach of any applicable law, for your negligent or willful misconduct.</p>
                <p>7. You understand and agree that the relationship between Sactpro and you will not be considered as an employer and employee relation.</p>
                <p>8. You understand that the final decision to choose a particular student for tuition is yours and yours alone. In case you are selected in a demo, you can’t reject the tuition until and unless there are authentic reason. In the event where you are unable to attend the tuition, a prior notice of 48 hours before the lesson starts, must be given to the students/parents.</p>
                <p>9. You understand and agree that you will teach the student if and only if at least one parent is present in the house. If you are leaving any tuition, you must intimate us prior to 1 week. Sactpro will not be liable for any conflict or disagreement between the tutors and students/ parents. We will also not be responsible for any form of damage caused in connection with your use of our website.</p>
                <p>10. The subscription charges for tutor is only Rs 500 /- and it's valid for three months. The subscription charges are non- refundable under any circumstances. Sactpro does not guarantee to secure tuition jobs for tutors. Sactpro will charge you an upfront  30% of   1st month tuition fee as a commission upon student/parent’s confirmation. The subsequent month's 100% of tuition fee will be yours. You authorize Sactpro to collect payment on your behalf in consideration of the home tuition delivered. You agree that all the tax liability for this payment is yours. You understand and agree that Sactpro will deduct its commission from the payment received.</p>

                <p>11. The tutor must have convincing teaching skills and good behaviour with student & parents. Selected Tutors do not share their selected tuition details with their friends / colleagues and do not invite them to the student's residence at the time of introduction / Demo class.</p>
                <p>12. A tutor here by acknowledges that Sactpro are not liable to the Tutor for reimbursement of any fees paid pursuant to these Terms in the events of any cancellation of any Contract or Tuition for any reason.</p>
                <p>13. You here by confirm that you will abide all the rules and regulations of Sactpro. And in case if you get caught cheating with Sactpro then Sactpro can take any legal action on you. Any attempt to cheat or deprive Sactpro.com of its legal share of commission, will result in legal actions being taken.</p>
                <p>14. Sactpro reserves all the rights to change all or any of these above mentioned Terms and Conditions with or without giving any prior notice at any time.</p>
                <p>15. You may not solicit, collect, give or use the login credentials of other users of the Service.</p>
                <p>16. You may not impersonate any other person or entity or to use a false name or a name that you have no authority to use.</p>
                <p></p>
            </div>
        </div>
    </div>
</div>

<?php include_once("Footer.php"); ?>
