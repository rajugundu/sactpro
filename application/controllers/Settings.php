<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function edit($id) {
		// retreiveing data from selected div using id
		$retrieve_id = $id;
		$data = $this->db->get_where('student_registration', array('id' => $retrieve_id));
		$data_results = $data->row();
		$data_array = json_decode(json_encode($data_results), true);
		$data_username = $data_array['username'];
		$data_number = $data_array['number'];
		$data_email = $data_array['email'];
		$data_class = $data_array['class'];
		$data_subject = $data_array['subject'];
		$data_address = $data_array['address'];
		$data_city = $data_array['city'];

		// updating coins using username

		$username = $_SESSION['username'];
		$users = $this->db->get_where('users', array('username' => $username));
    $results = $users->row();
    $array = json_decode(json_encode($results), true);
    $amount = $array['paid'];
		$number = $array['number'];


		$prev_amount = $amount; // Deducting user coins after subscribing
		$deduct_amount = '100';
		$total = $prev_amount - $deduct_amount;

		$insert_data = array (
        'paid' => $total,
    );

		$this->db->set('paid', $total);
    $this->db->where('username', $username);
    $this->db->update('users');

		// printing required information
		// Inserting user with subscribed student $student_details
		$latest_insert_data = array (
				'user_username ' => $username,
				'user_phone' => $number,
				'student_username ' => $data_username,
				'student_number ' => $data_number,
				'student_email ' => $data_email,
				'student_class ' => $data_class,
				'student_subject ' => $data_subject,
				'student_address ' => $data_address,
				'student_city ' => $data_city,
    );
		$this->db->insert('coins_subscribe', $latest_insert_data);

		// $this->load->view('Tutor_dashboard/Thank_you');
		$this->session->set_flashdata('Purchased', 'You have successfully acquired student details. Please view details in "My Requirements". ');

		redirect("User/tutor_profile", 'refresh');



	}



		function __construct(){
      parent::__construct();
    }

      public function do_multi_upload(){
        if($this->input->post('submit_form')){
           if($_FILES["profile_pic"]["tmp_name"]) {
            $this->do_miltiupload_files('public/uploads', 'profile_image', $_FILES['profile_pic']); // call the function with params
           }
         }
       }

    	function do_miltiupload_files($path, $title, $files) {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|gif|png',
            'overwrite'     => 1,
            'remove_spaces' => 0,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['multi_images[]']['name']= $files['name'][$key];
            $_FILES['multi_images[]']['type']= $files['type'][$key];
            $_FILES['multi_images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['multi_images[]']['error']= $files['error'][$key];
            $_FILES['multi_images[]']['size']= $files['size'][$key];
            // here we change file name on run time
            $fileName = $title .'_'. $image;
            $images[] = $fileName;
            $config['file_name'] = $fileName; //new file name
            $this->upload->initialize($config); // load new config setting
            $upload_data[] = $this->upload->data();

            if ($this->upload->do_upload('multi_images[]')) { // upload file here

              $username = $_SESSION['username'];

              $insert_data = array (
								't_state' => $_POST['t_state'],
								'board' => implode(',', $_POST['board']),
								'mode' => implode(',', $_POST['mode']),
								'teach-plc' => implode(',', $_POST['teach-plc']),
								// 'teach_inst' => $_POST['teach_inst'],
								'class' => implode(',', $_POST['class']),
								'subject' => $_POST['subject'],
								'gender' => $_POST['gender'],
								'teach-exp' => $_POST['teach-exp'],
								'address' => $_POST['address'],
								'city' => $_POST['city'],
								'state' => $_POST['state'],
								'pincode' => $_POST['pincode'],
								'photo' => $upload_data[0]['full_path'],

								// 'subject' => $_POST['subject'],

              );
              $this->db->where('username', $username);
              $this->db->update('users', $insert_data);

              redirect('User/tutor_profile', 'refresh');
              // redirect('', 'refresh');

							$this->session->set_flashdata('updated', 'Your Profile has been updated. Please login again');


            } else {
                return false;

            }
        }
        // return $images;
    }

}
?>
