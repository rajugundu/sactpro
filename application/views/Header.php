<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <title><?php echo $pageTitle; ?></title>
    <meta name="description" content="<?php echo $pageDescription; ?>">
    <meta charset="utf-8">
    <meta name="keywords" content="City Home Tuition, Home Tuition, Tutor Jobs, Private Tutors, Tutor, Tutors, Sactpro, Sac, Tuitions, Students, Computers Tuitions, find tutors nearby, hire best tutor">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="canonical" href="<?php echo $pageCanonical;?>">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117921139-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117921139-1');
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
$(window).load(function(){
  $('#NewmyModal').modal('show');
});
</script>



    <link rel="icon" href="<?=base_url()?>public/img/favicon.png">
    <link rel="stylesheet" href="<?=base_url()?>public/css/style.css" crossorigin="anonymous">

    <link rel="stylesheet" href="<?=base_url()?>public/bootstrap/css//bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  </head>
  <body>

  <div class="addon-top-header">
    <div class="container-fluid">
      <p><span><img style="width: 15px;" src="<?=base_url()?>public/img/icons/phone.svg" /></i><a style="color: white;" href="tel:9182374217"> 9182374217</a></span>
      <span><img style="width: 15px;" src="<?=base_url()?>public/img/icons/email.svg" /> <a style="color: white;" href=mailto:info@sactpro.com>info@sactpro.com</a></span></p>
    </div>
  </div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Login</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#tutor">TUTOR</a></li>
        <li><a data-toggle="tab" href="#student">STUDENT</a></li>
      </ul>
      <div class="tab-content">
            <div id="tutor" class="tab-pane fade in active">
              <!-- <h4>TUTOR</h4> -->
              <?php echo form_open_multipart('Login/index');?>
                  <div class="form-group">
                      <!-- <label for="username">Full Name</label> -->
                      <input type="text" class="form-control" id="username" name="username" placeholder="Tutor username" required autofocus>
                  </div>
                  <div class="form-group">
                      <!-- <label for="password">Password</label> -->
                      <input type="password" class="form-control" id="password" name="password" placeholder="Password" required autofocus>
                  </div>
                  <p>Forgot password? <a class="forgot-password" href="">Reset here</a></p>
                  <div class="modal-footer">
              <button type="submit" class="btn btn-primary" name="login">Login</button>
            </div>
          </form>
        </div>
        <div id="student" class="tab-pane fade">

        <!-- <h4>STUDENT LOGIN</h4> -->
        <?php echo form_open_multipart('Login/index');?>
            <div class="form-group">
                <!-- <label for="username">Full Name</label> -->
                <input type="text" class="form-control" id="username" name="username" placeholder="Student username" required autofocus>
            </div>
            <div class="form-group">
                <!-- <label for="password">Password</label> -->
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required autofocus>
            </div>
            <p>Forgot password? <a class="forgot-password" href="">Reset here</a></p>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary" name="login_student">Login</button>
            </div>
        </form>
        </div>
      </div>



      </div>

      <!-- Modal footer -->

    </div>
  </div>
</div>




  <div class="top-header">
    <div class="container-fluid">


<nav class="navbar navbar-expand-lg navbar-light">
  <div class="main-logo">
  <a href="<?=base_url()?>"><img src="<?=base_url()?>public/img/main_logo.png" /></a>
  </div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="col-lg-5 col-md-2 col-sm-2">
    </ul>
    <ul class="navbar-nav col-lg-4 col-md-6 col-sm-6">
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>about">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>contact">Contact Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>faq">Faq</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>Donate" title="Donate">Donate</a>
      </li>
    </ul>
    <form class="form-inline my-2 col-lg-3 col-md-4 col-sm-4">
      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Login</button>
      <a class="btn btn-primary" href="<?=base_url()?>register">REGISTER</a>
    </form>
  </div>
</nav>


</div>

</div>
