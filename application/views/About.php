<?php
// Define variables for SEO
$pageTitle = 'About | SactPro - Find tutor online';
$pageDescription = 'About Us. Learn who we are and what we do at sactpro';
$pageCanonical = 'http://sactpro.com/about';
 ?>
<?php include_once("Header.php"); ?>

<div class="about-us">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4>SACT PRO - About Us</h4>
                <p>SAC Tuitions is India's largest and most trusted Learning Network. Our vision is to be one-stop learning partner for every Indian. With Verified Tutors, Trainers & Schools, we are a trusted partner of choice for more students, parents and professionals visiting us every month to fulfill learning requirements across more than 1000 courses.</p>
                <p>Using sac Tuitions, students, parents and professionals can compare multiple Tutors, Trainers and Institutes and choose the ones that best suit their requirements.</p>
                <p>If you are a Tutor, Trainer or an Institute, you can get relevant enquiries based on your skills and offer online as well as offline coaching services.</p>
            </div>
        </div>
    </div>
</div>

<?php include_once("Footer.php"); ?>
